extends Panel

const LIST_ITEM_FMT = "%d. %s [%s]"

const COMMANDS = [
	["about", "about. aliases: ?"],
	["ck [nth]", "copy item passkey. aliases: cpkey"],
	["e [nth]", "edit item. aliases: edit"],
	["bosskey", "change boss key. aliases: changekey, masterkey"],
	["passgen", "generate passkeys"],
	["q", "quit. aliases: exit, quit"]
]
const FILTERS = [
	["dup-keys", "duplicate keys"],
	["key <key>", "a particular key"],
	["old-keys <yyymmdd>", "old keys"]
]

const DUMMY_ICON_H = 32

var dummy_icon = null

func _ready():
	set_process_input(true) # NOTE: see _input()

	SetupMenu()

	get_node("new-pass-btn").connect("pressed", self, "SwitchSceneToEditNew")

	var list = get_node("item-list")
	list.outer = self

	var dummy_icon = ImageTexture.new()
	dummy_icon.create(0, self.DUMMY_ICON_H, Image.FORMAT_RGBA)
	self.dummy_icon = dummy_icon

	var filter_input = get_node("filter-input")
	filter_input.grab_focus()
	filter_input.connect("text_changed", self, "ParseFilterInput")
	filter_input.connect("text_entered", self, "DoCommand")
	filter_input.set_text(G.filter_text)
	filter_input.set_cursor_pos(G.filter_text.length())
	filter_input.set_placeholder_alpha(0.25)
	ParseFilterInput(G.filter_text)

func _input(ev):
	if ev.type == InputEvent.KEY:
		if ev.pressed:
			# NOTE: filter input key input stuff
			var item_list = get_node("item-list")
			var filter_input = get_node("filter-input")
			if item_list.has_focus():
				if (ev.control and ev.scancode == KEY_N) or ev.scancode == KEY_DOWN:
					var index = item_list.GetSelectedItemIndex()+1
					if index == item_list.get_item_count():
						index -= 1
					item_list.select(index)
					item_list.ensure_current_is_visible()
					get_tree().set_input_as_handled()

				elif (ev.control and ev.scancode == KEY_P) or ev.scancode == KEY_UP:
					var index = item_list.GetSelectedItemIndex()-1
					if index == -2:
						return

					if index == -1:
						get_node("filter-input").grab_focus()
					else:
						item_list.select(index)
					item_list.ensure_current_is_visible()
					get_tree().set_input_as_handled()

				elif (ev.control and ev.scancode == KEY_M) or ev.scancode == KEY_RIGHT or ev.scancode == KEY_ENTER or ev.scancode == KEY_RETURN:
					var index = item_list.GetSelectedItemIndex()
					SwitchSceneToEdit(index, true)
					get_tree().set_input_as_handled()

				elif ev.control and ev.scancode == KEY_C:
					var index = item_list.GetSelectedItemIndex()
					if index == -1:
						return
					CopyItemPassKey(index)
					get_tree().set_input_as_handled()

				elif ev.scancode == KEY_ESCAPE:
					var index = item_list.GetSelectedItemIndex()
					if index == -1:
						return
					item_list.unselect(index)
					get_node("filter-input").grab_focus()
					get_tree().set_input_as_handled()

			elif filter_input.has_focus():
				if (ev.control and ev.scancode == KEY_N) or ev.scancode == KEY_DOWN:
					item_list.grab_focus()
					item_list.select(0)
					get_tree().set_input_as_handled()

				elif ev.control and ev.scancode == KEY_M:
					filter_input.emit_signal("text_entered", filter_input.get_text())
					get_tree().set_input_as_handled()


func SwitchSceneToEditNew(store_filter_text=false):
	if store_filter_text:
		StoreFilterText()
	G.SetScene("res://scenes/edit.tscn", [null])

func SwitchSceneToEdit(index, store_filter_text=false):
	if store_filter_text:
		StoreFilterText()
	var edit_item = get_node("item-list").get_item_metadata(index)
	G.SetScene("res://scenes/edit.tscn", [edit_item])

func ShowMenu():
	get_node("menu-btn").get_popup().popup()

func StoreFilterText():
	G.filter_text = get_node("filter-input").get_text()

func ParseFilterInput(text=""):
	var list = get_node("item-list")
	list.clear()

	var filter_input = get_node("filter-input")
	# NOTE: resetting placeholder and tooltip text whenever any (new) input
	# happens essentially
	if text != "":
		filter_input.set_placeholder("")
		filter_input.set_tooltip("")

	var working_set = Util.ArrayShallowCopy(G.data.items)

	var parts = text.split(";")

	for i in range(parts.size()):
		parts[i] = parts[i].strip_edges()

		if parts[i].begins_with("/") or parts[i].begins_with("#"):
			# NOTE: begins with command or msg prefix. don't do anything (except
			# when enter is pressed for command prefix)
			pass
		elif parts[i].begins_with("("):
			if parts[i][parts[i].length()-1] == ")":
				DoMacroFilter(working_set, parts[i])
		elif not parts[i].empty():
			DoIncrementalFilter(working_set, parts[i])

	var n = 0
	for i in range(working_set.size()):
		AddListItem(working_set[i])

func AddListItem(item):
	var list = get_node("item-list")
	var n = list.get_item_count()
	var label = self.LIST_ITEM_FMT % [n+1, item.label, item.username]
	list.add_item(label, self.dummy_icon)
	list.set_item_metadata(n, item)

func DoIncrementalFilter(working_set, text):
	var i = 0
	while i < working_set.size():
		var item = working_set[i]
		if item.label.findn(text) == -1 and item.username.findn(text) == -1:
			working_set.remove(i)
		else:
			i += 1

func DoMacroFilter(working_set, text):
	text = text.substr(1, text.length()-2)

	var macro_args = Array(text.split(" "))
	var macro = macro_args[0]
	macro_args.pop_front()
	var argc = macro_args.size()

	if macro == "duplicate-keys" or macro == "dup-keys":
		print("checking for duplicate keys...")
		var new_working_set = []
		var i = 0
		while i < working_set.size():
			var base_item = working_set[i]
			var ii = i+1
			while ii < working_set.size():
				if base_item.passkey == working_set[ii].passkey:
					if not base_item in new_working_set:
						new_working_set.push_back(base_item)
					if not working_set[ii] in new_working_set:
						new_working_set.push_back(working_set[ii])
				ii += 1
			i += 1
		working_set.clear()
		# NOTE: by providing a dest array to ArrayShallowCopy, it will not
		# create a new array, and instead copy the source directly into the
		# dest
		Util.ArrayShallowCopy(new_working_set, working_set)

	elif macro == "key":
		if argc != 1:
			DisplayNumArgsError(macro, 1, argc)
			return
		var i = 0
		while i < working_set.size():
			var item = working_set[i]
			if item.passkey != macro_args[0]:
				working_set.remove(i)
				i -= 1
			i += 1

	elif macro == "notes":
		var i = 0
		while i < working_set.size():
			var item = working_set[i]
			if item.notes.empty():
				working_set.remove(i)
				i -= 1
			i += 1

	# NOTE: for now, must have one argument: a date in the [yyyy][mm][dd]
	# number format. e.g.: 20160101
	elif macro == "old-keys":
		if argc != 1:
			DisplayNumArgsError(macro, 1, argc)
			return
		var out_date = macro_args[0].to_int()
		var i = 0
		while i < working_set.size():
			var item = working_set[i]
			if item.passdate > out_date:
				working_set.remove(i)
				i -= 1
			i+=1

	elif macro == "order":
		# TODO: order items in list, e.g. alpha, modified, created
		pass

	elif macro == "weak-keys":
		# TODO: display what the app thinks are items with 'weak' keys. first,
		# need to research this topic
		pass

func DoCommand(cmd_text):
	var cmd_prefix_pos = cmd_text.find("/")
	if cmd_prefix_pos == -1:
		if get_node("item-list").get_item_count() > 0:
			# NOTE: enter was pressed to edit the first item in the list
			SwitchSceneToEdit(0)

		return

	cmd_text = cmd_text.right(cmd_prefix_pos+1)

	var cmd_args = Array(cmd_text.split(" "))
	var cmd = cmd_args[0]
	cmd_args.pop_front()
	var argc = cmd_args.size()

	var cmd_input = get_node("filter-input")

	# NOTE: start of commands

	if cmd == "about" or cmd == "?":
		ShowAbout()

	elif cmd == "bak" or cmd == "backup" or cmd == "dbmanage":
		#G.SetScene("res://scenes/dbmanage.tscn")
		pass

	elif cmd == "cfg":
		pass

	elif cmd == "ck" or cmd == "cpkey":
		var ith = 1
		if argc == 1:
			ith = cmd_args[0].to_int()
		var item_list = get_node("item-list")
		if ith <= 0 or ith > item_list.get_item_count():
			DisplayCmdError(cmd, "bad arg '%s'" % cmd_args[0])
		else:
			CopyItemPassKey(ith-1)

	elif cmd == "e" or cmd == "edit":
		var ith = 1
		if argc == 1:
			ith = cmd_args[0].to_int()
		var item_list = get_node("item-list")
		if ith <= 0 or ith > item_list.get_item_count():
			DisplayCmdError(cmd, "bad arg '%s'" % cmd_args[0])
		else:
			SwitchSceneToEdit(ith-1)

	elif cmd == "export":
		G._ExportBossKeyData()

	elif cmd == "import":
		if argc == 0:
			G._ImportBossKeyData(self)
		elif argc == 1:
			if cmd_args[0] == "bosskey":
				G._ImportBossKeyData(self)
			elif cmd_args[0] == "lastpass":
				G._ImportLastPassCsv(self)
			else:
				var fmt = "bad arg '%s'"
				DisplayCmdError(cmd, fmt % cmd_args[0])
		else:
			DisplayNumArgsError(cmd, "0 or 1", argc)

	elif cmd == "bosskey" or cmd == "changekey" or cmd == "masterkey":
		G.SetScene("res://scenes/changepass.tscn")

	elif cmd == "n" or cmd == "new":
		SwitchSceneToEditNew()

	elif cmd == "passgen":
		G.SetScene("res://scenes/passgen.tscn", ["res://scenes/list.tscn"])

	elif cmd == "q" or cmd == "quit" or cmd == "exit":
		get_tree().quit()

	# NOTE: this handles command errors, as well as numerical commands that are
	# a shortcut to edit the nth item. e.g.: /3 --> edit 3rd item in current
	# working set
	else:
		var num_items = get_node("item-list").get_item_count()
		# NOTE: if a string has a number in it, then .to_int() will return
		# that number somehow. if it has no numbers (e.g. "asdf"), then it 
		# returns 0
		var i = cmd.to_int()
		if cmd != "0" and i == 0:
			DisplayCmdError(cmd)
		elif i <= 0 or i > num_items:
			DisplayCmdError(cmd)
		else:
			SwitchSceneToEdit(i-1)

	# NOTE: have to clear input at the end, because the input control sends a
	# signal to ParseFilterInput when text changes. was previously doing this
	# earlier in the func, but it somehow was still working. now, the signal
	# seems to be triggering the func immediately, resetting the filtered items
	# back to the unfiltered state, and thus was causing some problems with
	# subsequent commands that operated on the filtered list
	cmd_input.clear()

func DisplayMsg(msg):
	var filter_input = get_node("filter-input")
	var dmsg = "# %s" % msg
	filter_input.clear()
	filter_input.set_placeholder(dmsg)
	filter_input.set_tooltip(dmsg)

func DisplayCmdError(cmd, msg="not found"):
	var fmt = "# err: '%s' - %s"
	var err_msg = fmt % [cmd, msg]
	var filter_input = get_node("filter-input")
	filter_input.set_placeholder(err_msg)
	filter_input.set_tooltip(err_msg)

func DisplayNumArgsError(cmd, expected, actual):
	expected = str(expected)
	actual = str(actual)
	var fmt = "# err: num args mismatch. expected %s, got %s"
	DisplayCmdError(cmd, fmt % [expected, actual])

func SetupMenu():
	var menu = get_node("menu-btn").get_popup()
	menu.set_custom_minimum_size(Vector2(200, 100))

	var import_submenu = CreateImportSubMenu(menu)
	var export_submenu = CreateExportSubMenu(menu)
	var commands_submenu = CreateCommandsSubMenu(menu)
	var filters_submenu = CreateFiltersSubMenu(menu)

	menu.add_submenu_item("import", import_submenu.get_name(), 0)
	menu.add_submenu_item("export", export_submenu.get_name(), 1)
	menu.add_separator()
	menu.add_item("passgen", 2)
	menu.add_separator()
	menu.add_submenu_item("commands", commands_submenu.get_name(), 3)
	menu.add_submenu_item("filters", filters_submenu.get_name(), 4)
	menu.add_separator()
	menu.add_item("about", 5)
	menu.add_separator()
	menu.add_item("quit", 6)

	menu.connect("item_pressed", self, "HandleMenu")

func HandleMenu(id):
	if id == 0:
		pass

	elif id == 1:
		pass

	if id == 2:
		# NOTE: open passgen
		StoreFilterText()
		G.SetScene("res://scenes/passgen.tscn", ["res://scenes/list.tscn"])

	elif id == 3:
		pass

	elif id == 4:
		pass

	elif id == 5:
		ShowAbout()

	elif id == 6:
		get_tree().quit()

func CreateImportSubMenu(parent):
	var submenu = PopupMenu.new()
	submenu.set_name("import")
	submenu.connect("item_pressed", self, "HandleImportSubMenu")

	submenu.add_item("BossKey", 0)
	submenu.add_item("LastPass", 1)

	parent.add_child(submenu)

	return submenu

func HandleImportSubMenu(id):
	if id == 0:
		var cfg = {
			filter = "*.json;JSON",
			mode = FileDialog.MODE_OPEN_FILE,
			access = FileDialog.ACCESS_FILESYSTEM
		}
		Util.PopupFileDialog(self, "ImportBossKeyData", cfg)

	elif id == 1:
		var cfg = {
			filter = "*.csv;Comma Separated Values",
			mode = FileDialog.MODE_OPEN_FILE,
			access = FileDialog.ACCESS_FILESYSTEM
		}
		Util.PopupFileDialog(self, "ImportLastPassCsv", cfg)

func CreateExportSubMenu(parent):
	var submenu = PopupMenu.new()
	submenu.set_name("export")
	submenu.connect("item_pressed", self, "HandleExportSubMenu")

	submenu.add_item("JSON", 0)

	parent.add_child(submenu)

	return submenu

func HandleExportSubMenu(id):
	if id == 0:
		G._ExportBossKeyData()

func CreateCommandsSubMenu(parent):
	var submenu = PopupMenu.new()
	submenu.set_name("commands")
	submenu.connect("item_pressed", self, "HandleCommandsSubMenu")

	for id in range(self.COMMANDS.size()):
		submenu.add_item("/%s" % self.COMMANDS[id][0], id)

	parent.add_child(submenu)

	return submenu

func HandleCommandsSubMenu(id):
	var cmd = self.COMMANDS[id][0].insert(0, "/")
	var filter_input = get_node("filter-input")
	filter_input.set_text(cmd)

	var space_pos = cmd.find(" ")
	if space_pos > -1:
		filter_input.select(space_pos+1, -1)

	filter_input.set_cursor_pos(cmd.length())

func CreateFiltersSubMenu(parent):
	# CONSDIDER: add a 'show all filter macros' item, which either goes to
	# another scene or popups up some scrollable list
	var submenu = PopupMenu.new()
	submenu.set_name("filters")
	submenu.connect("item_pressed", self, "HandleFiltersSubMenu")

	for id in range(self.FILTERS.size()):
		submenu.add_item("(%s)" % self.FILTERS[id][0], id)

	parent.add_child(submenu)

	return submenu

func HandleFiltersSubMenu(id):
	# NOTE: not adding the closing paren so that the filter doesn't run as soon
	# as the text is inserted. could otherwise lead to problems if placeholder
	# args are part of the insertion
	var fmt = "(%s"
	var macro = fmt % self.FILTERS[id][0]
	var filter_input = get_node("filter-input")
	# NOTE: clearing selection of filter-input, if any, to avoid any potential
	# confusion
	filter_input.select(0, 0)

	var space_pos = macro.find(" ")
	if space_pos == -1:
		# NOTE: no arguments, so go ahead and close the macro so that it will
		# run
		macro = macro.insert(macro.length(), ")")

	filter_input.set_text(macro)

	if space_pos > -1:
		filter_input.select(space_pos+1, -1)

	filter_input.set_cursor_pos(macro.length())

	# NOTE: going ahead and parse filter input in case macro doesn't take
	# arguments
	ParseFilterInput(macro)

func ResetFilter():
	get_node("filter-input").clear()
	ParseFilterInput()

func ImportBossKeyData(path):
	G.ImportBossKeyData(path)
	ResetFilter()

func ImportLastPassCsv(path):
	G.ImportLastPassCsv(path)
	ResetFilter()

func ShowAbout():
	var cfg = { title = "about" }
	var fmt = "BossKey version %s (%d) - by acalc\n\nTwitter: @patternmind\nEmail: <acalc@protonmail.com>"
	Util.PopupAcceptDialog(fmt % [G.SEMANTIC_VERSION, G.DATE_VERSION], cfg)

func CopyItemPassKey(item_index):
	var item = get_node("item-list").get_item_metadata(item_index)
	OS.set_clipboard(item.passkey)
	DisplayMsg("copied item %d's passkey" % (item_index+1))
	get_node("filter-input").grab_focus()
