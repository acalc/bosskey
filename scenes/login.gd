extends Panel

func _ready():
	set_process_unhandled_key_input(true)

	get_node("open-btn").connect("pressed", self, "OpenDb")
	get_node("pass-input").grab_focus()

func _unhandled_key_input(ev):
	if ev.pressed and (ev.scancode == KEY_RETURN or ev.scancode == KEY_ENTER):
		OpenDb()

func OpenDb():
	G.bosskey = get_node("pass-input").get_text().sha256_buffer()
	var err = G.OpenDb()
	if err == 0:
		G.SetScene("res://scenes/list.tscn")
	elif err == 16:
		var cfg = {
			title = "error",
			confirmed_obj = self,
			confirmed_fn = "RefocusPassInput"
		}
		Util.PopupAcceptDialog("incorrect master passkey :(", cfg)
	else:
		print("Error opening passdb (code: " + str(err) + ")")

func RefocusPassInput():
	var pass_input = get_node("pass-input")
	pass_input.clear()
	pass_input.grab_focus()
