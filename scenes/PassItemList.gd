extends ItemList

# NOTE: have to manually set the outer class (in the outer class) after this
# class is instanced
var outer = null

var target_item_index = -1

func _ready():
	connect("focus_exit", self, "__handle_focus_exit")

func _input_event(ev):
	if ev.type == InputEvent.MOUSE_BUTTON and ev.button_index == BUTTON_LEFT:
		if ev.pressed:
			self.target_item_index = get_item_at_pos(ev.pos)
		else:
			var item_index = get_item_at_pos(ev.pos)
			if item_index != self.target_item_index:
				UnselectItem()
				return
			
			if ev.control and item_index != null:
				self.outer.call("CopyItemPassKey", item_index)
			else:
				self.outer.call("SwitchSceneToEdit", item_index, true)

			self.target_item_index = -1

	accept_event()

func __handle_focus_exit():
	UnselectItem()


func UnselectItem():
	var index = GetSelectedItemIndex()
	if index == -1:
		return
	unselect(index)

func GetSelectedItemIndex():
	# NOTE: get_selected_items returns IntArray of item indices
	var selected_items = get_selected_items()
	if selected_items.size() == 0:
		return -1
	else:
		return selected_items[0]
