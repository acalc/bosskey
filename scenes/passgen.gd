extends Panel

const DEF_WORDLIST = "eff_small.txt"

var word_list = null
var prev_scene = null
var prev_custom_seed_text = ""
var enter_repeating = false

func _ready():
	set_process_input(true)

	var pass_output = get_node("pass-output")
	pass_output.set_readonly(true)
	pass_output.set_wrap(true)

	var password_tab_btn = get_node("password-tab-btn")
	var passphrase_tab_btn = get_node("passphrase-tab-btn")
	password_tab_btn.connect("pressed", self, "ShowPasswordTab")
	passphrase_tab_btn.connect("pressed", self, "ShowPassphraseTab")
	get_node("generate-btn").connect("pressed", self, "GeneratePass")
	get_node("copy-btn").connect("pressed", self, "CopyGeneratedPass")
	get_node("back-btn").connect("pressed", self, "GotoPrevScene")

	var wordlists_optbtn = get_node("passphrase-tab/wordlists-optbtn")
	var files = Util.GetItemsInDir("res://res/wordlists/", [], true)["files"]
	if files != null:
		for filename in files:
			wordlists_optbtn.add_item(filename)
	wordlists_optbtn.connect("item_selected", self, "LoadWordList")

	var custom_seed_checkbox = get_node("custom-seed-checkbox")
	custom_seed_checkbox.connect("toggled", self, "ToggleCustomSeedInput")

	LoadCfg()

	# NOTE: LoadCfg has determined the wordlist to load
	LoadWordList(wordlists_optbtn.get_selected())

	get_node("generate-btn").grab_focus()

func _input(ev):
	if ev.type == InputEvent.KEY:
		if ev.scancode == KEY_ESCAPE:
			GotoPrevScene()


func ShowPasswordTab():
	get_node("passphrase-tab-btn").set_pressed(false)
	get_node("password-tab-btn").set_pressed(true)
	get_node("passphrase-tab").hide()
	get_node("password-tab").show()

	get_node("pass-output").set_text("")
	get_node("generate-btn").grab_focus()

func ShowPassphraseTab():
	get_node("password-tab-btn").set_pressed(false)
	get_node("passphrase-tab-btn").set_pressed(true)
	get_node("password-tab").hide()
	get_node("passphrase-tab").show()

	get_node("pass-output").set_text("")
	get_node("generate-btn").grab_focus()

func LoadWordList(index):
	var f = File.new()
	var path = "res://res/wordlists/%s"
	var err = f.open(path % get_node("passphrase-tab/wordlists-optbtn").get_item_text(index), File.READ)
	if err == 0:
		self.word_list = f.get_as_text().split("\n")
		f.close()

func CopyGeneratedPass():
	var pk = get_node("pass-output").get_text()
	OS.set_clipboard(pk)
	get_node("copy-btn").set_text("copied " + str(pk.length()) + " characters!")

func GeneratePass():
	var using_custom_seed = get_node("custom-seed-checkbox").is_pressed()
	var custom_seed_input = get_node("custom-seed-input")
	var custom_seed_text = custom_seed_input.get_text()
	if using_custom_seed:
		if custom_seed_text.empty():
			var cfg = { title = "error" }
			Util.PopupAcceptDialog("custom seed can't be empty", cfg)
			return FAILED
		elif custom_seed_text != self.prev_custom_seed_text:
			seed(custom_seed_text.hash())
			self.prev_custom_seed_text = custom_seed_text
	else:
		randomize()

	SaveCfg()

	var passkey = RawArray()

	if get_node("password-tab-btn").is_pressed():
		var tab = get_node("password-tab")
		var num_AZ = tab.get_node("upper-alpha-spinbox").get_value()
		var num_az = tab.get_node("lower-alpha-spinbox").get_value()
		var num_09 = tab.get_node("0-9-spinbox").get_value()
		var num_charset = tab.get_node("charset-spinbox").get_value()

		for i in range(num_AZ):
			passkey.push_back(floor(rand_range(65.0, 90.99)))

		for i in range(num_az):
			passkey.push_back(floor(rand_range(97.0, 122.99)))

		for i in range(num_09):
			passkey.push_back(floor(rand_range(48.0, 57.99)))

		var charset = tab.get_node("charset-input").get_text().to_utf8()
		if charset.size() > 0:
			for i in range(num_charset):
				var c = randi() % (charset.size()-1)
				passkey.push_back(charset[c])

		var pk_size = passkey.size()
		for i in range(pk_size):
			var n = randi() % (pk_size-1)
			var a = passkey[i]
			var b = passkey[n]
			passkey[n] = a
			passkey[i] = b

	elif get_node("passphrase-tab-btn").is_pressed() and self.word_list != null:
		var num_gen_words = get_node("passphrase-tab/num-words-spinbox").get_value()
		# NOTE: subtracting 1 for blank last line in the wordlists
		var num_passphrase_words = self.word_list.size() - 1
		for i in range(num_gen_words):
			var r = randi() % (num_passphrase_words-1)
			var word = self.word_list[r].to_utf8()
			for ii in range(word.size()):
				passkey.push_back(word[ii])
			if i < num_gen_words-1:
				passkey.push_back(32)

	var pk = passkey.get_string_from_utf8()
	var pk_len = pk.length()
	get_node("pass-output").set_text(pk)
	var copy_btn = get_node("copy-btn")
	copy_btn.set_text("copy " + str(pk_len) + " characters")
	if pk_len > 0:
		copy_btn.set_disabled(false)

func GotoPrevScene():
	if self.prev_scene != null:
		SaveCfg()
		G.SetScene(self.prev_scene)

func SaveCfg():
	var cfg = ConfigFile.new()

	cfg.set_value("PASSGEN", "num_AZ", get_node("password-tab/upper-alpha-spinbox").get_value())
	cfg.set_value("PASSGEN", "num_az", get_node("password-tab/lower-alpha-spinbox").get_value())
	cfg.set_value("PASSGEN", "num_09", get_node("password-tab/0-9-spinbox").get_value())
	cfg.set_value("PASSGEN", "num_charset", get_node("password-tab/charset-spinbox").get_value())
	cfg.set_value("PASSGEN", "charset", get_node("password-tab/charset-input").get_text())
	cfg.set_value("PASSGEN", "wordlist", get_node("passphrase-tab/wordlists-optbtn").get_selected())
	cfg.set_value("PASSGEN", "num_passphrase", get_node("passphrase-tab/num-words-spinbox").get_value())

	var err
	if OS.is_debug_build():
		err = cfg.save("user://bosskey_debug.cfg")
	else:
		err = cfg.save("user://bosskey.cfg")

func LoadCfg():
	var cfg = ConfigFile.new()

	var err
	if OS.is_debug_build():
		err = cfg.load("user://bosskey_debug.cfg")
	else:
		err = cfg.load("user://bosskey.cfg")

	get_node("password-tab/upper-alpha-spinbox").set_value(cfg.get_value("PASSGEN", "num_AZ", 5))
	get_node("password-tab/lower-alpha-spinbox").set_value(cfg.get_value("PASSGEN", "num_az", 5))
	get_node("password-tab/0-9-spinbox").set_value(cfg.get_value("PASSGEN", "num_09", 4))
	get_node("password-tab/charset-spinbox").set_value(cfg.get_value("PASSGEN", "num_charset", 2))
	get_node("password-tab/charset-input").set_text(cfg.get_value("PASSGEN", "charset", "~!@#$%^&*_+=-./?:|"))

	# NOTE: selecting (and soon loading) the previously used wordlist
	var wordlists_optbtn = get_node("passphrase-tab/wordlists-optbtn")
	var def_wordlist_index = 0
	for i in range(wordlists_optbtn.get_item_count()):
		if wordlists_optbtn.get_item_text(i) == self.DEF_WORDLIST:
			def_wordlist_index = i
			break
	wordlists_optbtn.select(cfg.get_value("PASSGEN", "wordlist", def_wordlist_index))

	get_node("passphrase-tab/num-words-spinbox").set_value(cfg.get_value("PASSGEN", "num_passphrase", 6))

func ToggleCustomSeedInput(pressed):
	var custom_seed_input = get_node("custom-seed-input")
	custom_seed_input.set_editable(not custom_seed_input.is_editable())
	custom_seed_input.clear()
