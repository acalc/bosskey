extends Panel

func _ready():
	set_process_unhandled_key_input(true)
	get_node("create-btn").connect("pressed", self, "CreateDb")

	get_node("newpass/new-pass-input").grab_focus()

func _unhandled_key_input(ev):
	if ev.pressed and (ev.scancode == KEY_RETURN or ev.scancode == KEY_ENTER):
		CreateDb()


func CreateDb():
	var newpass = get_node("newpass")

	var code = newpass.Validate(true)
	if code == 0:
		var passkey = newpass.GetPass(true)
		G.bosskey = passkey
		G.SetScene("res://scenes/list.tscn")
