extends Node

func _ready():
	set_process_input(true)

	get_node("confirm-btn").connect("pressed", self, "ChangePass")
	get_node("cancel-btn").connect("pressed", self, "CancelChange")

	get_node("oldpass-input").grab_focus()

func _input(ev):
	if ev.type == InputEvent.KEY:
		if ev.scancode == KEY_ESCAPE and ev.pressed:
			G.SetScene("res://scenes/list.tscn")


func ChangePass():
	var newpass = get_node("newpass")

	var prev_key = get_node("oldpass-input").get_text().sha256_buffer()
	if prev_key != G.bosskey:
		var cfg =  { title = "error" }
		Util.PopupAcceptDialog("previous seems to be incorrect. try again", cfg)
		return

	var code = newpass.Validate(true)
	if code == 0:
		G.CommitPassData("keychange")

		var passkey = newpass.GetPass(true)
		# NOTE: setting a new passkey causes the database to be re-encoded with it
		G.SetNewPassKey(passkey)
		G.SetScene("res://scenes/list.tscn")

func CancelChange():
	G.SetScene("res://scenes/list.tscn")
