extends Panel

var new_item = false
var item

func _ready():
	set_process_input(true)
	LoadItem()

	get_node("back-btn").connect("pressed", self, "GotoList")
	get_node("delete-btn").connect("pressed", self, "DeleteItem")
	get_node("save-btn").connect("pressed", self, "SaveItem")
	get_node("pass-widget/pass-view-btn").connect("pressed", self, "TogglePassView")
	get_node("pass-widget/copy-btn").connect("pressed", self, "CopyPass")

	get_node("label-input").grab_focus()
	get_node("notes-input").set_wrap(true)

	if not self.new_item:
		get_node("back-btn").grab_focus()

func _input(ev):
	if ev.type == InputEvent.KEY:
		# NOTE: a bit of a hack-ish way to bypass TextEdit's seemingly
		# unavoidable handling of the tab button
		if ev.scancode == KEY_TAB and ev.pressed and get_node("notes-input").has_focus():
			get_tree().set_input_as_handled()
			var notes_input = get_node("notes-input")
			call_deferred("FocusFromTextInput", notes_input.get_text(), notes_input, get_node("save-btn"))

		elif ev.scancode == KEY_ESCAPE and ev.pressed:
			GotoList()


func FocusFromTextInput(text, text_input, focus_control):
	text_input.set_text(text)
	focus_control.grab_focus()

func LoadItem():
	if self.item == null:
		print("creating new pass item")
		self.new_item = true
		self.item = G.NewItem()
	else:
		self.new_item = false
		get_node("label-input").set_text(self.item.label)
		get_node("username-input").set_text(self.item.username)
		get_node("pass-widget/pass-input").set_text(self.item.passkey)
		get_node("url-input").set_text(self.item.url)
		get_node("notes-input").set_text(self.item.notes)

func TogglePassView():
	var secret = true
	if get_node("pass-widget/pass-view-btn").is_pressed():
		secret = false
	get_node("pass-widget/pass-input").set_secret(secret)

func GotoList():
	G.SetScene("res://scenes/list.tscn")

func SaveItem():
	assert(self.item != null)

	var label = get_node("label-input").get_text().strip_edges()

	if label.empty():
		var cfg = { title = "error" }
		Util.PopupAcceptDialog("Can't save the item without a label!", cfg)
		return

	self.item.label = label
	self.item.url = get_node("url-input").get_text()
	self.item.username = get_node("username-input").get_text()
	var oldpass = self.item.passkey
	self.item.passkey = get_node("pass-widget/pass-input").get_text()
	self.item.notes = get_node("notes-input").get_text()

	var prevkeys = self.item.prevkeys
	if oldpass != self.item.passkey:
		prevkeys.push_front(oldpass)
		if prevkeys.size() == G.MAX_PREV_KEYS+1:
			prevkeys.pop_back()

	if self.new_item:
		G.data.items.push_back(self.item)

	G.CommitPassData()
	GotoList()

func DeleteItem():
	var cfg
	var msg
	
	if self.new_item:
		msg = "Deleting a new item will just discard it. Proceeed?"
		cfg = {
			title = "delete new item",
			confirmed_obj = self,
			confirmed_fn = "GotoList",
			cancel_btn_text = "cancel"
		}
	else:
		msg = "This deletion will be permanent. Proceed?"
		cfg = {
			title = "delete item",
			confirmed_obj = self,
			confirmed_fn = "DeleteConfirmed",
			cancel_btn_text = "cancel"
		}
		
	Util.PopupAcceptDialog(msg, cfg)

func CopyPass():
	OS.set_clipboard(get_node("pass-widget/pass-input").get_text())

func DeleteConfirmed():
	G.data.items.erase(self.item)
	G.CommitPassData()
	GotoList()
