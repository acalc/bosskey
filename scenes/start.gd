extends Node

func _ready():
	if OS.get_name() != "Android":
		OS.set_low_processor_usage_mode(true)

	var db = File.new()
	if db.file_exists(G.passdb_path):
		G.SetScene("res://scenes/login.tscn")
	else:
		G.SetScene("res://scenes/newdb.tscn")
