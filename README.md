BossKey -- a simple, cross-platform password manager

email: acalc@protonmail.com

Password info is encrypted and stored locally and is not currently stored or 
backed-up in the cloud. Also, it currently does not integrate with a web browser,
making it a more manual password management experience. However, it's (being) 
designed to quickly access, add, delete, and change password info with a 
keyboard or mouse.

Currently, this software has not been widely tested or audited. With regards to 
data security and integrity, use it at your own discretion. If you don't feel
comfortable using it as an everyday tool, feel free to help audit its security
or help test it out for bugs and general user experience.

###How to build

BossKey is built with Godot Engine. If you prefer not to run any [prebuilt
binaries](https://bitbucket.org/acalc/bosskey/downloads), here's how to build 
from source using Godot 2.1:

1. download [Godot Engine](https://godotengine.org/download)
2. download the export templates, from the same page. REMINDER: BossKey is
currently being developed with Godot 2.1 stable
3. run Godot Engine and open the BossKey project
4. click on the 'Settings' menu item (located at the top-right), then click
'install export templates'. Navigate to the *.tpz file that contains the
templates and click okay
5. now click the 'Export' menu item at the top-left. From here, it should be
fairly simple, as the project should already be basically configured to be
built for Windows, OSX, and Linux. Just select the platform and click 
'Export...' and choose a binary/zip output location. Refer to [this
documentation](http://docs.godotengine.org/en/latest/tutorials/asset_pipeline/exporting_projects.html) for more info on exporting Godot projects if you want/need
to.

NOTE: BossKey uses the open source font Hack. The font's binary and readme -- 
taken from its [GitHub repo](https://github.com/chrissimpkins/Hack) -- are 
located in the res/fonts/ folder. UPDATE: As of Godot 2.1, dynamic fonts are 
possible. That means that the fonts the app uses are no longer statically 
generated at design time (by me) from the source font file, but rather the source
is used directly.

###How to backup the app's database

BossKey stores passkeys and related data in one file, `passdb`, in the virtual 
`user://` directory that Godot Engine defines. The actual location of this 
differs from platform to platform. In the future, I may implement a better, more 
user-friendly way to create/manage backups. But for now, it has to be done 
manually.

So, if you want to manually backup the app's database on OSX or Linux, you could 
do something like this:

`cp ~/.godot/app_userdata/bosskey/passdb /path/to/put/backup/passdb`

As for Windows, I believe it is stored where app userdata is typically stored, 
although I am not exactly sure at the moment (I don't use Windows).

Backing up is useful, especially before using a new build of BossKey. It
will actually automatically create backups in the `user://` directory in a few 
cirumstances right now:

1. when you import LastPass or other BossKey data
2. when you change your masterkey
