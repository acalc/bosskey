# NOTE: global variables and functions that are availabe to every scene

extends Node

# NOTE: this is metadata used to determine how the passdb will be saved, loaded,
# imported, and exported in the future
const DATA_FORMAT = 1
const DATE_VERSION = 20161121
const SEMANTIC_VERSION = "1.0.0"

const MAX_PREV_KEYS = 3

var scenes
var bosskey
var passdb_path
var filter_text = ""

var metadata = { data_format = DATA_FORMAT, app_date_version = DATE_VERSION, app_semantic_version = SEMANTIC_VERSION }
var data = { items = [] }

func _ready():
	self.scenes = {}
	self.scenes["res://scenes/list.tscn"] = preload("res://scenes/list.tscn")
	self.scenes["res://scenes/edit.tscn"] = preload("res://scenes/edit.tscn")

	if OS.is_debug_build():
		self.passdb_path = "user://passdb_debug"
	else:
		self.passdb_path = "user://passdb"


func _ExportBossKeyData():
	var cfg = {
		filter = "*.json;JSON",
		mode = FileDialog.MODE_SAVE_FILE,
		access = FileDialog.ACCESS_FILESYSTEM
	}
	Util.PopupFileDialog(self, "ExportBossKeyData", cfg)
func ExportBossKeyData(path):
	var filename = path.get_file()
	if filename == "" or filename == ".json":
		var cfg = {	title = "export error" }
		Util.PopupAcceptDialog("invalid or missing filename", cfg)
		return

	if path.extension() != "json":
		path = path.insert(path.length(), ".json")
	var f = File.new()
	var err = f.open(path, File.WRITE)
	if err == 0:
		var output = { metadata = self.metadata, data = self.data }
		f.store_string(output.to_json())
		f.close()

func ImportBossKeyData(path):
	var f = File.new()
	var err = f.open(path, File.READ)
	if err != 0:
		var cfg = { title = "import error" }
		Util.PopupAcceptDialog("Could not open %s" % path, cfg)
		return

	var bk_data = {}
	var parse_err = bk_data.parse_json(f.get_as_text())
	if parse_err != 0:
		var cfg = {
			title = "import error",
			size = Vector2(300, 200)
		}
		Util.PopupAcceptDialog("an error occurred attempting to parse the file's content as JSON. make sure you are trying to import a JSON file or that it is properly formatted, and try again.", cfg)
		return

	# NOTE: backing up data before commiting new imported items
	CommitPassData("import")

	# NOTE: this is the old json export format
	if not bk_data.has("data"):
		for import_item in bk_data.items:
			self.data.items.push_back(NewItem(import_item))

	else:
		for import_item in bk_data.data.items:
			if not IsDuplicateItem(import_item):
				self.data.items.push_back(NewItem(import_item))

	f.close()
	CommitPassData()

func SetScene(path, args=[]):
	# NOTE: deferring call due to having to unload current scene and because of
	# the timing of things in Godot
	call_deferred("_SetScene", path, args)
func _SetScene(path, args=[]):
	var new_scene = null
	if self.scenes.has(path):
		new_scene = self.scenes[path].instance()
	else:
		new_scene = load(path).instance()

	# NOTE: specific configuration stuff, depending on the scene being set
	if path == "res://scenes/edit.tscn":
		new_scene.item = args[0]
	elif path == "res://scenes/passgen.tscn":
		new_scene.prev_scene = args[0]

	var tree = get_tree()
	var current_scene = tree.get_current_scene()
	current_scene.free()
	tree.get_root().add_child(new_scene)
	tree.set_current_scene(new_scene)

func NewItem(from=null):
	var item = {}

	item.label = ""
	item.url = ""
	item.username = ""
	item.passkey = ""
	item.notes = ""

	var current_date = OS.get_date()
	var date_fmt = "%d%02d%02d"
	var date = (date_fmt % [date.year, date.month, date.day]).to_int()

	item.created = date
	item.modified = date
	item.passdate = date

	item.prevkeys = [""]

	# NOTE: copying *all* keys from 'from' item. this includes keys that may no
	# longer be in use. It's safer to go ahead and do that, and then cleanup
	# the passdb afterwards of unused keys
	if from != null:
		for fk in from.keys():
			item[fk] = from[fk]

	return item

func CommitPassData(backup_type=null):
	var path = null

	if backup_type != null:
		var fmt = "%s_backup_%s"
		path = fmt % [self.passdb_path, backup_type]
	else:
		path = self.passdb_path

	var metadata_f = File.new()
	var status = metadata_f.open("%s_metadata" % path, File.WRITE)
	metadata_f.store_var(self.metadata)
	metadata_f.close()

	var db_f = File.new()
	status = db_f.open_encrypted(path, File.WRITE, self.bosskey)
	db_f.store_var(self.data)
	db_f.close()

	return OK

func OpenDb():
	var metadata_f = File.new()
	if not metadata_f.file_exists("%s_metadata" % self.passdb_path):
		return FAILED

	var code = metadata_f.open("%s_metadata" % self.passdb_path, File.READ)
	if code > 0:
		return code
	self.metadata = metadata_f.get_var()
	metadata_f.close()

	# CONSIDER: using call and semantic version given in the metadata, call the
	# correct OpenDbx_x_x fn
	return OpenDb1_0_0()

func OpenDb1_0_0():
	var db_f = File.new()
	if not db_f.file_exists(self.passdb_path):
		return FAILED

	var code = db_f.open_encrypted(self.passdb_path, File.READ, self.bosskey)
	if code > 0:
		return code
	var data = db_f.get_var()
	db_f.close()

	for item in data.items:
		var new_item = NewItem(item)
		self.data.items.push_back(new_item)

	return OK

func SetNewPassKey(newpk):
	self.bosskey = newpk
	# NOTE: committing and encoding data with the new key
	CommitPassData()

func ImportLastPassCsv(path):
	var f = File.new()
	var err = f.open(path, File.READ)
	if err != 0:
		var cfg = { title = "import error" }
		Util.PopupAcceptDialog("Could not open %s" % path, cfg)
		return

	# NOTE: before actually adding imported items to self.data.items, adding them
	# to a temp buffer to make sure there's no error's parsing the CSV. if no
	# error's, then officially adding them to the app's data
	var blank_file = true
	var imported_items = []
	while true:
		var record = f.get_csv_line()

		if record[0].empty():
			if blank_file:
				print("error: blank csv file")
				return
			else:
				break

		if record.size() != 7:
			var cfg = { title = "import error" }
			Util.PopupAcceptDialog("format error: a LastPass record does not have 7 entries. Aborting the import...", cfg)
			return

		var item = NewItem()
		item.label = record[4]
		item.url = record[0]
		item.username = record[1]
		item.passkey = record[2]
		item.notes = record[3]
		imported_items.push_back(item)

		blank_file = false

	# NOTE: backing up passdb
	CommitPassData("import")

	for import_item in imported_items:
		if not IsDuplicateItem(import_item):
			self.data.items.push_back(NewItem(import_item))
			
	f.close()
	CommitPassData()

func IsDuplicateItem(test_item):
	for item in self.data.items:
		if test_item.label == item.label and test_item.url == item.url and test_item.username == item.username and test_item.passkey == item.passkey and test_item.notes == item.notes:
			return true

	return false
