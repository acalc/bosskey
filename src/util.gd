extends Node

const POPUP_CENTERED = 0
const POPUP_CENTERED_MINSIZE = 1
const POPUP_CENTERED_RATIO = 2

func PopupAcceptDialog(msg, cfg={}):
	var title = GetCfg(cfg, "title", "")
	var ok_btn_text = GetCfg(cfg, "ok_btn_text", "okay")
	var cancel_btn_text = GetCfg(cfg, "cancel_btn_text", "")
	var size = GetCfg(cfg, "size", Vector2(300, 150))
	var confirmed_callback = [GetCfg(cfg, "confirmed_obj", null), GetCfg(cfg, "confirmed_fn", ""), GetCfg(cfg, "confirmed_args", [])]
	var denied_callback = [GetCfg(cfg, "denied_obj", null), GetCfg(cfg, "denied_fn", ""), GetCfg(cfg, "denied_args", [])]

	# CONSIDER: calculate the width of the dialog by the width of the string
	var dialog = AcceptDialog.new()
	dialog.set_meta("confirmed", false)
	dialog.set_meta("confirmed_callback", confirmed_callback)
	dialog.set_meta("denied_callback", denied_callback)
	
	dialog.get_ok().set_text(ok_btn_text)
	if cancel_btn_text != "":
		dialog.add_cancel(cancel_btn_text)
	dialog.set_size(size)
	dialog.set_title(title)
	dialog.set_text(msg)
	dialog.set_exclusive(true)
	dialog.set_hide_on_ok(false)

	var label = dialog.get_label()
	label.set_align(Label.ALIGN_CENTER)
	label.set_valign(Label.VALIGN_CENTER)
	label.set_autowrap(true)

	ShowPopup(dialog, POPUP_CENTERED, size)

func PopupFileDialog(file_selected_obj, file_selected_fn, cfg={}):
	var title = GetCfg(cfg, "title", "")
	var init_dir = GetCfg(cfg, "init_dir", null)
	var mode = GetCfg(cfg, "mode", 0)
	var access = GetCfg(cfg, "access", FileDialog.ACCESS_FILESYSTEM)
	var filter = GetCfg(cfg, "filter", null)
	var show_hidden = GetCfg(cfg, "show_hidden", false)
	var def_filename = GetCfg(cfg, "def_filename", "")
	var size = GetCfg(cfg, "size", Vector2(375, 375))

	var dialog = FileDialog.new()

	dialog.set_exclusive(true)

	if init_dir != null:
		dialog.set_current_dir(init_dir)

	dialog.set_mode(mode)
	dialog.set_access(access)

	if filter != null:
		dialog.add_filter(filter)

	dialog.set_show_hidden_files(show_hidden)
	dialog.set_current_file(def_filename)
	#dialog.set_size(size)

	var sig
	if mode == FileDialog.MODE_OPEN_FILE or mode == FileDialog.MODE_SAVE_FILE:
		sig = "file_selected"
	elif mode == FileDialog.MODE_OPEN_FILES:
		sig = "files_selected"
	elif mode == FileDialog.MODE_OPEN_DIR:
		sig = "dir_selected"
	elif mode == FileDialog.MODE_OPEN_ANY:
		# NOTE: in order to do this, will need to connect at least two signals
		# below
		pass
	dialog.connect(sig, file_selected_obj, file_selected_fn)

	ShowPopup(dialog, POPUP_CENTERED, size)

func ShowPopup(popup, how, how_arg, bg_opacity=0.75):
	assert(how >= 0 and how <= 2)

	var root = get_node("/root")

	var bg = Panel.new()
	bg.set_opacity(bg_opacity)
	root.call_deferred("add_child", bg)
	bg.call_deferred("set_area_as_parent_rect")

	popup.connect("popup_hide", self, "__handle_popup_hide", [popup, bg])
	popup.connect("confirmed", self, "__set_confirmed", [popup])

	root.call_deferred("add_child", popup)

	var fn
	if how == 0:
		fn = "popup_centered"
	elif how == 1:
		fn = "popup_centered_minsize"
	elif how == 2:
		fn = "popup_centered_ratio"

	popup.call_deferred(fn, how_arg)

func __set_confirmed(popup):
	popup.set_meta("confirmed", true)
	popup.show_modal(false)

func __handle_popup_hide(popup, bg=null):
	if bg != null:
		bg.queue_free()	
	
	var callback = null
	
	if popup.has_meta("confirmed") and popup.get_meta("confirmed"):
		if popup.has_meta("confirmed_callback"):
			callback = popup.get_meta("confirmed_callback")
	else:
		if popup.has_meta("denied_callback"):
			callback = popup.get_meta("denied_callback")

	if callback != null and callback[0] != null and callback[1] != "":
		callback[0].callv(callback[1], callback[2])

	popup.queue_free()

# STATIC FUNCTIONS #

static func ArrayShallowCopy(source, dest=[]):
	for i in range(source.size()):
		dest.push_back(source[i])

	return dest

static func DictShallowCopy(source, dest={}):
	for k in source.keys():
		dest[k] = source[k]

	return dest

# CONSIDER: add option to prepend path to item names
static func GetItemsInDir(path, filters=[], ignore_hidden=false):
	var dir = Directory.new()
	var status = dir.open(path)
	if status != OK:
		return status

	var items = { all = [], files = [], folders = [], hidden = [] }
	dir.list_dir_begin()
	var item_name = dir.get_next()
	while item_name != "":
		var hidden_item = item_name.begins_with(".")
		if ignore_hidden and hidden_item:
			item_name = dir.get_next()
			continue

		if filters.empty():
			filters.push_back("*")

		for filter in filters:
			if item_name.match(filter):
				if dir.current_is_dir():
					items["folders"].push_back(item_name)
				else:
					items["files"].push_back(item_name)

				if hidden_item:
					items["hidden"].push_back(item_name)

				items["all"].push_back(item_name)
				break

		item_name = dir.get_next()

	return items

static func GetCfg(cfg, field, def):
	if cfg.has(field):
		return cfg[field]
	else:
		return def
