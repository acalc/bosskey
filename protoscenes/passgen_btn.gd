extends Button

func _ready():
	connect("pressed", self, "ShowPassGen")


func ShowPassGen():
	var current_scene_fname = get_tree().get_current_scene().get_filename()
	var args = [current_scene_fname, 2]
	G.SetScene("res://scenes/passgen.tscn", args)
