extends Control

const EMPTY_PASS = 1
const REPEAT_PASS_DIFFERENT = 2

func _ready():
	pass

# TODO: return int codes instead to indicate the various problems that could
# occur
func Validate(show_dialog=false):
	var passkey1 = get_node("new-pass-input")
	var passkey2 = get_node("repeat-pass-input")

	var accept_dialog = null

	if passkey1.get_text().empty():
		if show_dialog:
			var cfg =  { title = "error" }
			Util.PopupAcceptDialog("boss key can't be empty!", cfg)
		return self.EMPTY_PASS

	elif passkey1.get_text() != passkey2.get_text():
		if show_dialog:
			var cfg = { title = "error" }
			Util.PopupAcceptDialog("repeated boss key is not the same!", cfg)
		return self.REPEAT_PASS_DIFFERENT

	else:
		return 0

func SetPass(passkey):
	get_node("new-pass-input").set_text(passkey)
	get_node("repeat-pass-input").set_text(passkey)

func GetPass(as_key):
	var code = Validate()
	if code == 0:
		var p = get_node("new-pass-input").get_text()
		if as_key:
			# NOTE: this will end up returning 32 bytes, which is the required
			# len for the RawArray key in File.open_encrypted
			return p.sha256_buffer()
		else:
			return p
	else:
		return null
